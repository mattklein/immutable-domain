package io.thoughtfoundry

import java.util.UUID

import io.thoughtfoundry.model._
import org.scalatest._

class IdeaSpec extends FlatSpec with Matchers {
  "An Idea" should "be composable from a series of events" in {
    val idea = Idea.loadFromEvents(
      IdeaCreated(UUID.randomUUID(), Handle("Test")),
      IdeaDescriptionUpdated("Test description")
    )
    idea.handle shouldBe Handle("Test")
    idea.description shouldBe Some("Test description")
  }
}
