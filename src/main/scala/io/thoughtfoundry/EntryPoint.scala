package io.thoughtfoundry

import java.util.UUID

import io.thoughtfoundry.model._

object EntryPoint extends App {
  val user = User(Person("Matt", "Kleinschafer"))
  val author = Author(user)

  val idea = Idea
    .loadFromEvents(
      IdeaDraftCreated(UUID.randomUUID(), Handle("idea1")),
      IdeaCreated(UUID.randomUUID(), Handle("idea1")),
      IdeaBackgroundUpdated("Test background"),
      IdeaDescriptionUpdated("Test description")
    )
    .approve(user)
    .addComment(author, "Test comment 1")
    .addComment(author, "Test comment 2")
    .addComment(author, "Test comment 3")
    .addComment(author, "Test comment 4")
    .deny(user)

  println(idea)
}
