package io.thoughtfoundry.model

import io.thoughtfoundry.model.IdeaState._

// core entity and factory
case class Idea(id: Idea#ID,
                handle: Handle,
                state: IdeaState = New,
                description: Option[String] = None,
                background: Option[String] = None,
                auditor: Option[User] = None,
                likers: List[User] = Nil,
                voters: List[User] = Nil,
                comments: List[Idea#Comment] = Nil,
                uncommittedEvents: List[Event] = Nil
               ) extends Aggregate[Idea] {
  override def markCommitted(): Idea = copy(uncommittedEvents = Nil)

  override protected val eventHandler: EventHandler = {
    case event: IdeaCreated =>
      copy(
        state = state.transition(Create),
        uncommittedEvents = uncommittedEvents :+ event
      )
    case event: IdeaDescriptionUpdated =>
      copy(
        description = Some(event.description),
        uncommittedEvents = uncommittedEvents :+ event
      )
    case event: IdeaBackgroundUpdated =>
      copy(
        background = Some(event.background),
        uncommittedEvents = uncommittedEvents :+ event
      )
    case event: IdeaLiked =>
      state assert Open
      copy(
        likers = likers :+ event.user,
        uncommittedEvents = uncommittedEvents :+ event
      )
    case event: IdeaVoted =>
      state assert Open
      copy(
        voters = voters :+ event.user,
        uncommittedEvents = uncommittedEvents :+ event
      )
    case event: IdeaCommentAdded =>
      state assert Open
      copy(
        comments = comments :+ Comment(event.author, event.body),
        uncommittedEvents = uncommittedEvents :+ event
      )
    case event: IdeaCommentRemoved =>
      state assert Open
      copy(
        comments = comments diff List(comments(event.index)),
        uncommittedEvents = uncommittedEvents :+ event
      )
    case event: IdeaApproved =>
      copy(
        auditor = Some(event.user),
        state = state.transition(Approve),
        uncommittedEvents = uncommittedEvents :+ event
      )
    case event: IdeaDenied =>
      copy(
        auditor = Some(event.user),
        state = state.transition(Deny),
        uncommittedEvents = uncommittedEvents :+ event
      )
  }

  def addComment(author: Author, body: String): Idea = applyEvent(IdeaCommentAdded(author, body))
  def removeComment(comment: Idea#Comment): Idea = applyEvent(IdeaCommentRemoved(comments.indexOf(comment)))
  def approve(user: User): Idea = applyEvent(IdeaApproved(user))
  def deny(user: User): Idea = applyEvent(IdeaDenied(user))

  case class Comment(author: Author, body: String)
}

object Idea extends AggregateFactory[Idea] {
  override protected def applyOriginEvent(event: Event): Idea = event match {
    case event: IdeaDraftCreated => Idea(
      id = event.id,
      handle = event.handle,
      state = IdeaState.Draft
    )
    case event: IdeaCreated => Idea(
      id = event.id,
      handle = event.handle,
      state = IdeaState.New
    )
  }
}

// events
sealed trait IdeaEvent extends Event
case class IdeaDraftCreated(id: Idea#ID, handle: Handle) extends IdeaEvent
case class IdeaCreated(id: Idea#ID, handle: Handle) extends IdeaEvent
case class IdeaDescriptionUpdated(description: String) extends IdeaEvent
case class IdeaBackgroundUpdated(background: String) extends IdeaEvent
case class IdeaCommentAdded(author: Author, body: String) extends IdeaEvent
case class IdeaCommentRemoved(index: Int) extends IdeaEvent
case class IdeaLiked(user: User) extends IdeaEvent
case class IdeaVoted(user: User) extends IdeaEvent
case class IdeaApproved(user: User) extends IdeaEvent
case class IdeaDenied(user: User) extends IdeaEvent

// state machine
sealed trait IdeaTransition
case object Create extends IdeaTransition
case object Approve extends IdeaTransition
case object Deny extends IdeaTransition
case object Close extends IdeaTransition

sealed trait IdeaState extends StateMachine[IdeaState, IdeaTransition]
object IdeaState {
  case object Draft extends IdeaState {
    override val transitions: Transitions = {
      case Create => New
    }
  }
  case object New extends IdeaState {
    override val transitions: Transitions = {
      case Approve => Open
      case Close => Closed
      case Deny => Denied
    }
  }
  case object Open extends IdeaState {
    override val transitions: Transitions = {
      case Close => Closed
      case Deny => Denied
    }
  }
  case object Denied extends IdeaState
  case object Closed extends IdeaState
}
