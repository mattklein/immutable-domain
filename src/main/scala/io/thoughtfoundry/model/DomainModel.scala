package io.thoughtfoundry.model

import java.util.UUID

trait Command // TODO: writer pattern over an environment?
trait Event

trait Entity extends Product {
  type ID
  val id: ID

  override def hashCode(): Int = 31 * id.##
  override def equals(other: Any): Boolean = other match {
    case entity: Entity => entity.id == id
    case _ => false
  }
  override def toString: String = s"$productPrefix(id=$id)"
}

trait Aggregate[A <: Aggregate[A]] extends Entity {
  type ID = UUID
  type EventHandler = PartialFunction[Event, A]

  def uncommittedEvents: Seq[Event]
  def applyEvent(event: Event): A = eventHandler.applyOrElse(event, unhandledEvent)
  def unhandledEvent(event: Event): A = sys.error(s"An unhandled event was detected: $event")
  def markCommitted(): A

  protected val eventHandler: EventHandler
}

trait AggregateFactory[A <: Aggregate[A]] {
  def loadFromEvents(events: Event*): A = loadFromEvents(events)
  def loadFromEvents(events: Iterable[Event]): A = {
    var aggregate = applyOriginEvent(events.head)
    for (event <- events.tail) {
      aggregate = aggregate.applyEvent(event)
    }
    aggregate.markCommitted()
  }

  protected def applyOriginEvent(event: Event): A
}

trait StateMachine[State, Transition] {
  type Transitions = PartialFunction[Transition, State]

  def assert(state: State): Unit = if (this != state) sys.error(s"Expected to be in state: $state. Current state: $this")
  def transition(state: Transition): State = transitions.applyOrElse(state, failedTransition)
  def canTransition(state: Transition): Boolean = transitions.isDefinedAt(state)
  def failedTransition(state: Transition): State = sys.error(s"Cannot transition to $state")

  protected val transitions: Transitions = {
    case state => failedTransition(state)
  }
}

case class Handle(value: String) extends AnyVal
case class Person(firstName: String, lastName: String)
case class User(person: Person)
case class Author(user: User)
