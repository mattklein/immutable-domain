name := "immutable-domain"
version := "1.0"
scalaVersion := "2.11.9"

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "3.0.1" % "test"
)